<?php namespace Itul\RestApiConnector;

/**
*  The base class for the REST API Connector
*
*  Extend this class to connect to a REST API.
*
*  @author Troy L.
*/
class BaseClass{
	// consts
    /**  @var string|null $_endpoint stores the main api endpoint */
	protected $_endpoint = null;
    /**  @var string|null $_token stores the api token */
	protected $_token = null;
	//protected $_api_key = null;
    /**  @var array $_curl_headers stores the curl headers passed for each call */
	protected $_curl_headers = [];

	// auth
    /**  @var bool $_basic_auth whether or not to use basic curl authentication */
	protected $_basic_auth = false;
    /**  @var string $_username stores the api username */
	protected $_username = '';
    /**  @var string $_password stores the api password */
	protected $_password = '';

	// debug
    /**  @var array|null $_dump stores the call dump */
	protected $_dump = null;

	// settings
    /**  @var bool $parse_response_as_array parse the api call response as an array instead of an object */
	public $parse_response_as_array = false;

    /**
    * The main method used to make api requests
    *
    * @param string $method The HTTP method (POST, POSTGET, PUT, and GET can be used)
    * @param string $path The request path
    * @param array|null $data Any query data sent with the request. e.g. array("param" => "value")
    *
    * @return array Returns an array with http "code", http "response", and "error" keys
    */
	public function call($method,$path,$data = null){
		// init curl
		$c = curl_init();

		// set url
		$url = $this->_endpoint . $path;

		// set method
		switch($method){
			case "POST":
				curl_setopt($c,CURLOPT_POST,1);
				if($data) curl_setopt($c,CURLOPT_POSTFIELDS,$data);
				break;
			case "POSTGET":
				curl_setopt($c,CURLOPT_POST,1);
				if($data) curl_setopt($c,CURLOPT_POSTFIELDS,http_build_query($data));
				break;
			case "PUT":
				curl_setopt($c,CURLOPT_PUT,1);
				break;
			// case "DELETE":
			default: // GET
				if($data) $url = sprintf("%s?%s",$url,http_build_query($data));
		}

		// Optional Authentication:
		if($this->_basic_auth){
			curl_setopt($c,CURLOPT_HTTPAUTH,CURLAUTH_BASIC);
			curl_setopt($c,CURLOPT_USERPWD, $this->_username.":".$this->_password);
		}

		// set custom headers
		if(!empty($this->_curl_headers)){
			curl_setopt($c, CURLOPT_HTTPHEADER,$this->_curl_headers);
		}

		// init call
		curl_setopt($c,CURLOPT_URL,$url);
		curl_setopt($c,CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($c, CURLOPT_HEADER, 1);

		// exec call
		$result = curl_exec($c);
		$header_size = curl_getinfo($c, CURLINFO_HEADER_SIZE);
	    $http_code = curl_getinfo($c, CURLINFO_RESPONSE_CODE);
	    $error = curl_error($c);
		curl_close($c);

		// check http code
		if((100 <= $http_code) && ($http_code <= 199)){
	      	// info
	    } elseif((200 <= $http_code) && ($http_code <= 299)){
	      	// success
	    } elseif((300 <= $http_code) && ($http_code <= 399)){
	      	$error = $http_code.' Redirect';
	    } elseif((400 <= $http_code) && ($http_code <= 499)){
	      	$error = 'Client Error '.$http_code;
	    } elseif((500 <= $http_code) && ($http_code <= 599)){
	      	$error = 'Server Error '.$http_code;
	    }

		// check result
		if($result){
			// parse header and body
			$http_header = substr($result, 0, $header_size);
			$http_response = $this->parse_response(substr($result, $header_size));
		} else {
			// empty result
			if(!$error) $error = 'Result empty.';
			$http_header = null;
			$http_response = null;
		}

		// populate dump
		$request_data = compact('method','url','data');
		$response_data = compact('http_code','http_header','http_response','error');
		$this->_dump = compact('request_data','response_data');

		// return response
		$code = $http_code;
		$response = $http_response;
		return compact('code','response','error');
	}

	/**
    * Creates a unified response
    *
	* @param mixed $resp The response to parse.
	*
    * @return mixed
    */
	function parse_response($resp){
		return $this->is_json($resp) ? json_decode($resp,$this->parse_response_as_array):$resp;
	}

    /**
    * Fetches dump of latest request and response
	*
    * @return array Returns array with "request_data" and "response_data" keys
    */
	function get_dump(){
		return $this->_dump;
	}

    /**
    * This checks if a string is json or not.
    *
    * @param string $string The string to check.
    *
    * @return bool
    */
	function is_json($string) {
		try{
	      json_decode($string);
	      return (json_last_error() == JSON_ERROR_NONE);
	    } catch(Exception $e){
	      return false;
	    }
	}
}
